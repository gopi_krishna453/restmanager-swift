//
//  RestManager.swift
//
//  Created by Gopi K on 9/7/19.
//  Copyright © 2020 Gopi K. All rights reserved.
//

import UIKit
public protocol RestManagerProtocol: class {
    func successResponse(_ data: Data, _ serviceName:String)
    func failureResponse(_ error: Error, _ serviceName:String)
}

public class RestManager: NSObject {
    
    private var session: URLSession {
        let urlSessionConfig = URLSessionConfiguration.ephemeral
        urlSessionConfig.timeoutIntervalForResource = 20
        urlSessionConfig.timeoutIntervalForRequest = 20
        if #available(iOS 11.0, *) {
            urlSessionConfig.waitsForConnectivity = true
        } else {
            // Fallback on earlier versions
        }
        return URLSession(configuration: urlSessionConfig, delegate: self, delegateQueue: nil)
    }
    private var sessionTasks: [URLSessionTask]? = []
    private weak var serviceDelegate: RestManagerProtocol!
    private let BASE_URL = "https://jsonplaceholder.typicode.com/"
    public convenience init(delegate: RestManagerProtocol?) {
        self.init()
        self.serviceDelegate = delegate
    }
    
    private func getDefaulHeaders() -> HTTPHEADERS? {
        return ["Content-Type": "application/json", "Accept": "application/json"]
    }
    
    public func performTask(_ url:String, queryItems: [URLQueryItem]? = nil, task: TaskType = .data, with params: Encodable? = nil, httpMethod: HTTPMethod = .GET, httpheaders: HTTPHEADERS? = nil) {
        sessionTasks?.removeAll(where: { $0.taskDescription == url })
        do {
            let urlRequest = try createUrlRequest(url, queryItems, params, httpheaders, httpMethod.rawValue).getValue()
            var sessionTask: URLSessionTask?
            switch task {
            case .data : sessionTask = session.dataTask(with: urlRequest)
            case .download: sessionTask = session.downloadTask(with: urlRequest)
            }
            sessionTask?.taskDescription = url
            sessionTasks?.append(sessionTask!)
            sessionTask?.resume()
            
        } catch {
            serviceDelegate?.failureResponse(error, url)
            print(error)
        }
    }
    
    
    public func performSharedTask(_ url:String, queryItems: [URLQueryItem]? = nil, task: TaskType = .data, with params: Encodable? = nil, httpMethod: HTTPMethod = .GET, httpheaders: HTTPHEADERS? = nil,_ completion: @escaping (Result<Data>) -> ()) {
        sessionTasks?.removeAll(where: { $0.taskDescription == url })
        do {
            let urlRequest = try createUrlRequest(url, queryItems, params, httpheaders, httpMethod.rawValue).getValue()
            var sessionTask: URLSessionTask?
            let sharedSession = URLSession.shared
            let dataCompletion: (Data?, URLResponse?, Error?) -> Void = { (data, response, error) in
                completion(self.handleSharedTaskResponse(data, response, error))
            }
            let downloadCompletion: (URL?, URLResponse?, Error?) -> Void = { (locationUrl, response, error) in
                completion(self.handleSharedTaskResponse(try? Data(contentsOf: locationUrl!), response, error))
            }
            switch task {
            case .data :
                sessionTask = sharedSession.dataTask(with: urlRequest, completionHandler: dataCompletion)
            case .download: sessionTask = sharedSession.downloadTask(with: urlRequest, completionHandler: downloadCompletion)
            }
            sessionTask?.taskDescription = url
            sessionTasks?.append(sessionTask!)
            sessionTask?.resume()
            
        } catch {
            serviceDelegate?.failureResponse(error, url)
            print(error)
        }
    }
    
    
    
    private func createUrlRequest(_ urlString: String, _ queryItems: [URLQueryItem]?, _ params: Encodable? = nil, _ httpheaders: HTTPHEADERS? = nil, _ method: String) -> Result<URLRequest> {
        var urlComponents = URLComponents(string: BASE_URL + urlString)
        urlComponents?.queryItems = queryItems
        guard let url  = urlComponents?.url else {
            return .failure(ApiError.invalidUrl)
        }
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 20)
        if let params = params {
            do {
                urlRequest.httpBody = try params.toJSONData()
            } catch {
                return .failure(ApiError.jsonSerialisationError(error))
            }
        }
        urlRequest.httpMethod = method
        urlRequest.allHTTPHeaderFields = httpheaders ?? getDefaulHeaders()
        print("\(url) HTTPHEADERS:\(httpheaders as AnyObject), PARAMS : \(String(describing: (try? urlRequest.httpBody?.toJSONDictionary() as? NSDictionary) ?? [:]))")
        return .success(urlRequest)
    }
    
    public func cancelTasks(for services: [String]) {
        sessionTasks?.forEach({
            if services.compactMap({ $0 }).contains($0.taskDescription ?? "N/A") {
                $0.cancel()
            }
        })
    }
    
    private func handleTaskResponse(_ session: URLSession, _ task: URLSessionTask, _ data: Data?) {
        let serverURL:String = task.taskDescription ?? "No Task Description"
        sessionTasks?.removeAll(where: {  $0.taskDescription == task.taskDescription })
        session.finishTasksAndInvalidate()
        guard let response = task.response else {
            serviceDelegate?.failureResponse(ApiError.invalidResponse, serverURL)
            return
        }
        guard let responseData = data, !responseData.isEmpty else {
            serviceDelegate?.failureResponse(ApiError.dataDoesNotExist("noData"), serverURL)
            return
        }

        do {
            let _ = try response.validateHttpResponse().getValue()
            serviceDelegate?.successResponse(responseData, serverURL)
        } catch {
            serviceDelegate?.failureResponse(error, serverURL)
        }
    }
    
    private func handleSharedTaskResponse(_ data: Data? = nil, _ response: URLResponse?, _ error: Error?) -> Result<Data> {
        guard let response = response else {
            return .failure(ApiError.invalidResponse)
        }
        guard let responseData = data, !responseData.isEmpty else {
            return .failure(ApiError.dataDoesNotExist("noData"))
        }
        do {
            let _ = try response.validateHttpResponse().getValue()
            return .success(responseData)
        } catch {
            return .failure(error)
        }
    }
    
}

extension RestManager: URLSessionDataDelegate {
    
    public func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        self.handleTaskResponse(session, dataTask, data)
    }
            
}

extension RestManager: URLSessionDownloadDelegate {
    
    public func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        self.handleTaskResponse(session, downloadTask, try? Data(contentsOf: location))
    }
    
    public func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        session.finishTasksAndInvalidate()
        sessionTasks?.removeAll(where: {  $0.taskDescription == task.taskDescription })
        if let error = error {
            print("task: \(task.taskDescription ?? "No description") didCompleteWithError: \(error.localizedDescription)")
            serviceDelegate?.failureResponse(error, task.taskDescription ?? "")
        }
    }
}

extension RestManager: URLSessionDelegate {
    
    /// Did Receive Challange for any SSL Trust
    /*
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Swift.Void) {
        if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust {
            if let trust = challenge.protectionSpace.serverTrust,
                let certificate = Bundle.main.path(forResource: "filename", ofType: "cer"),
                let certificateData = NSData(contentsOfFile: certificate),
                let secCertificate = SecCertificateCreateWithData(nil, certificateData) {
                let secCertificates = [secCertificate]
                SecTrustSetAnchorCertificates(trust, secCertificates as CFArray)
                completionHandler(.useCredential, URLCredential(trust: trust))
                return
            }
        }
        // Pinning failed
        completionHandler(.cancelAuthenticationChallenge, nil)
    }
 */
}




