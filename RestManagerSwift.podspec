Pod::Spec.new do |s|
  s.name             = 'RestManagerSwift'
  s.version          = '0.1.0'
  s.summary          = 'Seamless Api Calls in swift wih URLSessions.'

  s.description      = <<-DESC
Network calls made simple using native URLSession Api, protocols and generics. 
                       DESC

  s.homepage         = 'https://gitlab.com/gopi_krishna453/restmanager-swift'
  s.license          = { :type => 'MIT', :file => 'LICENSE.txt' }
  s.author           = { 'Gopi krishna' => 'gopikrishna1812@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/gopi_krishna453/restmanager-swift.git', :tag => s.version.to_s }
  s.swift_version = '4.0'


  s.ios.deployment_target = '10.0'
  s.source_files = '*.swift'

end