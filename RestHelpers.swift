//
//  RestHelpers.swift
//
//  Created by Gopi K on 07/01/20.
//  Copyright © 2020 Gopi K. All rights reserved.
//

import Foundation

public typealias HTTPHEADERS = [String : String]
public enum HTTPMethod: String {
    case GET, POST, PUT, UPDATE
}

public enum TaskType {
    case data, download
}

public enum ApiError: Error {
    case dataDoesNotExist(String), jsonSerialisationError(Error), requestError(String), invalidUrl, invalidResponse, serverError(String), noNetworkConnection, noLocation, customError(String)
}

extension ApiError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .dataDoesNotExist(let value):
            return NSLocalizedString(value, comment: "dataDoesNotExist")
        case .jsonSerialisationError(let error):
            return NSLocalizedString(error.localizedDescription, comment: "jsonSerialisationError")
        case .requestError(let error):
            return NSLocalizedString(error, comment: "requestError")
        case .invalidUrl:
            return NSLocalizedString("invalidUrl", comment: "Invalid Url")
        case .invalidResponse:
            return NSLocalizedString("invalidResponse", comment: "Invalid Response")
        case .serverError(let value):
            return NSLocalizedString(value, comment: "serverError")
        case .noNetworkConnection:
            return NSLocalizedString("The Internet connection appears to be offline.", comment: "noNetworkConnection")
        case .noLocation:
            return NSLocalizedString("Spares Not Available", comment: "Spares Not Available")
        case .customError(let value):
            return NSLocalizedString(value, comment: "customError")
        }
    }
}

public enum Result<T> {
    case success(T), failure(Error)
    
    /// Returns the success value from the result
    ///
    /// - Returns: success value
    /// - Throws: error
    public func getValue() throws -> T {
        switch self {
        case .success(let value): return value
        case .failure(let error): throw error
        }
    }
}



extension URLResponse {
    
    public func validateHttpResponse() -> Result<HTTPURLResponse> {
        guard let httpResponse = self as? HTTPURLResponse else {
            return .failure(ApiError.invalidResponse)
        }
        print("httpResponse: \( String(describing: httpResponse))")
        guard (200...300) ~= httpResponse.statusCode else {
            var errorValue = HTTPURLResponse.localizedString(forStatusCode: httpResponse.statusCode)
            switch httpResponse.statusCode {
            case 404:
                errorValue = "Service Unavailable"
            case 500:
                errorValue = "Service Unavailable"
            default: break;
            }
            return .failure(ApiError.serverError(errorValue))
        }
        return .success(httpResponse)
    }
}

extension Data {
    public func toJSONDictionary() throws -> Any {
        return try JSONSerialization.jsonObject(with: self, options: .allowFragments)
    }
    
    public func toJSONString() -> String {
        return String.init(data: self, encoding: .utf8) ?? "Json String not found"
    }
}
extension Encodable {
    public func toJSONData() throws -> Data {
        let encoder = JSONEncoder()
        return try encoder.encode(self)
    }
}

extension Dictionary {
    public func toJSONData() throws -> Data {
        return try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
    }
}



